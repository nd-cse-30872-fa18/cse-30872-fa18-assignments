#!/bin/sh

for i in $(seq 0 11); do
    n=$(printf "%02d" $i)
    mkdir reading$n

    cat > reading$n/README.md <<EOF
Reading $n
==========

https://www3.nd.edu/~pbui/teaching/cse.30872.fa18/reading$n.html
EOF
done


for i in $(seq 0 24); do
    n=$(printf "%02d" $i)
    mkdir challenge$n

    cat > challenge$n/README.md <<EOF
Challenge $n
============

https://www3.nd.edu/~pbui/teaching/cse.30872.fa18/challenge$n.html
EOF
done
